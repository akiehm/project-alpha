from django.conf import settings
from django.db import models

# Create your models here.
USER_MODEL = settings.AUTH_USER_MODEL


class Project(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField()
    members = models.ManyToManyField(USER_MODEL, related_name="projects")
    start_date = models.DateField(null=True, blank=True)
    due_date = models.DateField(null=True, blank=True)
    is_completed = models.BooleanField(default=False)
    notes = models.TextField(
        null=True,
        blank=True,
        help_text="Add project retrospective notes here.",
    )

    def __str__(self):
        return self.name
