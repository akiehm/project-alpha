from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic.list import ListView

from projects.models import Project

# Create your views here.


class ProjectListView(LoginRequiredMixin, ListView):
    model = Project
    template_name = "projects/list.html"

    def get_queryset(self):
        return Project.objects.filter(members=self.request.user)


class ProjectDetailView(LoginRequiredMixin, DetailView):
    model = Project
    template_name = "projects/detail.html"


class ProjectCreateView(LoginRequiredMixin, CreateView):
    model = Project
    template_name = "projects/new.html"
    fields = ["name", "description", "members", "start_date", "due_date"]

    def get_success_url(self):
        return reverse_lazy("show_project", args=[self.object.id])


class ProjectUpdateView(LoginRequiredMixin, UpdateView):
    model = Project
    template_name = "projects/detail.html"
    fields = ["is_completed"]

    def get_success_url(self):
        return reverse_lazy("show_project", args=[self.object.id])


class ProjectUpdateView2(LoginRequiredMixin, UpdateView):
    model = Project
    template_name = "projects/detail.html"
    fields = ["notes"]

    def get_success_url(self):
        return reverse_lazy("show_project", args=[self.object.id])
