from django.urls import path

from projects.views import (
    ProjectListView,
    ProjectDetailView,
    ProjectCreateView,
    ProjectUpdateView,
    ProjectUpdateView2,
)


urlpatterns = [
    path("", ProjectListView.as_view(), name="list_projects"),
    path("<int:pk>/", ProjectDetailView.as_view(), name="show_project"),
    path("create/", ProjectCreateView.as_view(), name="create_project"),
    path(
        "<int:pk>/complete/",
        ProjectUpdateView.as_view(),
        name="complete_project",
    ),
    path(
        "<int:pk>/notes/",
        ProjectUpdateView2.as_view(),
        name="project_notes",
    ),
]
